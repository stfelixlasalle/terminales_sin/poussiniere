# Mini-projet Poussinière

Ceci est une base de code client/serveur fournie aux élèves de terminale SIN dans le cadre d'un mini-projet consistant à programmer un distributeur de nourriture connecté ayant pour but d'automatiser le suivi des besoins croissants de nourriture des poussins.

